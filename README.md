### About Okapi Filters Plugin for OmegaT

The **Okapi Filters Plugin for OmegaT** project provides plugins for the **open-source** translation tool [OmegaT](http://www.omegat.org/ OmegaT).

For more information about the Okapi Filters Plugin for OmegaT, see the
[corresponding page on the main wiki](http://okapiframework.org/wiki/index.php?title=Okapi_Filters_Plugin_for_OmegaT).

Bug report and enhancement requests: https://gitlab.com/okapiframework/omegat-plugin/-/issues

### Build status

* `main` branch: [![pipeline status](https://gitlab.com/okapiframework/omegat-plugin/badges/main/pipeline.svg)](https://gitlab.com/okapiframework/omegat-plugin/commits/main)
* `release` branch: [![pipeline status](https://gitlab.com/okapiframework/omegat-plugin/badges/release/pipeline.svg)](https://gitlab.com/okapiframework/omegat-plugin/commits/release)

### Downloads

* The latest stable version of the Okapi Filters Plugin for OmegaT is at https://okapiframework.org/binaries/omegat-plugin/
* The nightly version of the Okapi Filters Plugin for OmegaT is at https://gitlab.com/okapiframework/omegat-plugin/-/jobs/artifacts/dev/browse/deployment/done?job=verification