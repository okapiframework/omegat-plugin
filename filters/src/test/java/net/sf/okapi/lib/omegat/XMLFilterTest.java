package net.sf.okapi.lib.omegat;

import org.junit.Before;
import org.junit.Test;
import org.omegat.filters2.FilterContext;
import org.omegat.filters2.ITranslateCallback;
import org.omegat.util.Language;

import java.nio.file.Path;
import java.nio.file.Files;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class XMLFilterTest {

	XMLFilter filter;

    @Before
    public void setUp() {
        filter = new XMLFilter();
    }

    @Test
    public void testXmlFilterGotUnwantedTag() throws Exception {
        String source =  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<file>\n" +
                "    <items>\n" +
                "        <item id=\"i633174c4b6ced169305347574e94bd083\">\n" +
                "            <label key=\"i633174c4b6ced169305347574e94bd083_734b58c90f04a6eea63a38fe05615870_1\">\n" +
                "                <text>Look at Step 4.</text>\n" +
                "            </label>\n" +
                "        </item>\n" +
                "    </items>\n" +
                "</file>";
        String translation = "<g1>Popatrz na Etap 4.</g1>";
        String result = doTranslate(source, translation);
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<file>\n" +
                "    <items>\n" +
                "        <item id=\"i633174c4b6ced169305347574e94bd083\">\n" +
                "            <label key=\"i633174c4b6ced169305347574e94bd083_734b58c90f04a6eea63a38fe05615870_1\">\n" +
                "                <text>&lt;g1&gt;Popatrz na Etap 4.&lt;/g1&gt;</text>\n" +
                "            </label>\n" +
                "        </item>\n" +
                "    </items>\n" +
                "</file>";
        assertEquals(expected, result);
    }

    private String doTranslate(String source, String translation) throws Exception {
        Path tempDir = Files.createTempDirectory("okapi");
        Path inFile = tempDir.resolve("infile.xml");
        Path outFile = tempDir.resolve("outfile.xml");
        Files.writeString(inFile, source);
        tempDir.toFile().deleteOnExit();
        FilterContext fc = new FilterContext(new Language("en"), new Language("es"), false);
        ITranslateCallback cb = new TestTranslateCallback(translation);
        filter.translateFile(inFile.toFile(), outFile.toFile(), Collections.emptyMap(), fc, cb);
        return Files.readString(outFile);
    }

    static class TestTranslateCallback implements ITranslateCallback {
        private final String translation;

        TestTranslateCallback(String translation) {
            this.translation = translation;
        }

        @Override
        public void setPass(int i) {
        }

        @Override
        public void linkPrevNextSegments() {
        }

        @Override
        public String getTranslation(String s, String s1, String s2) {
            return translation;
        }

        @Override
        public String getTranslation(String s, String s1) {
            return translation;
        }
    }
}
