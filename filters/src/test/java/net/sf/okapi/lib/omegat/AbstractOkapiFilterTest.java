package net.sf.okapi.lib.omegat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotationType;
import net.sf.okapi.common.annotation.GenericAnnotations;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.TagType;
import net.sf.okapi.common.resource.TextUnit;

import org.junit.Test;
import org.omegat.filters2.FilterContext;
import org.omegat.util.Language;

public class AbstractOkapiFilterTest {
	@Test
	public void testComments () {
		XLIFFFilter filter = new XLIFFFilter();
		ITextUnit tu = new TextUnit("i1");
		TextFragment tf = new TextFragment("a ");
		tf.append(TagType.OPENING, "b", "<b>");
		tf.append("b");
		tf.append(TagType.CLOSING, "b", "</b>");
		tf.append(" cd e");
		// "a ##b## cd e"
		GenericAnnotations anns = new GenericAnnotations(
			new GenericAnnotation(GenericAnnotationType.TERM,
				GenericAnnotationType.TERM_INFO, "info",
				GenericAnnotationType.TERM_CONFIDENCE, 12.34));
		anns.add(new GenericAnnotation(GenericAnnotationType.TA,
				GenericAnnotationType.TA_CLASS, "REF:classURI"));
		tf.annotate(2, 7, GenericAnnotationType.GENERIC, anns);
		// "a ####b#### cd e"
		
		tf.annotate(12, 14, GenericAnnotationType.GENERIC, new GenericAnnotations(
			new GenericAnnotation(GenericAnnotationType.TA,
				GenericAnnotationType.TA_CLASS, "REF:classURI",
				GenericAnnotationType.TA_IDENT, "REF:identURI")));
		tu.setSource(new TextContainer(tf));
		
		String comments = filter.processComments(tu, false, true, tu.getSource().getSegments().get(0), null, "final");
		assertEquals("state = FINAL\n"
			+ "Term: 'b' info Confidence=12.34\n"
			+ "TA: 'b' Class:REF:classURI\n"
			+ "TA: 'cd' Class:REF:classURI Ident:REF:identURI",
			comments);
	}

	@Test
	public void testDoxygenProcess () throws Exception {
		DoxygenFilter filter = new DoxygenFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/javadoc-style.h").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out");
		outFile.delete();

		filter.parseFile(inFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		filter.translateFile(inFile, outFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());
	}
	
	@Test
	public void testXLIFFProcess_TranslateNoSpacePreserve () throws Exception {
		XLIFFFilter filter = new XLIFFFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/translate-no_space-preserve.xlf").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out");
		outFile.delete();

		filter.parseFile(inFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		//assertEquals("Sample document", omegat.getEntrySource(0));
		filter.translateFile(inFile, outFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());
		
		assertTrue(Utils.findInFile(outFile, "target xml:lang=\"fr-fr\">   Some data ["));
	}

	@Test
	public void testXLIFFProcess_SrcEqualsTrg () throws Exception {
		XLIFFFilter filter = new XLIFFFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/allowTransEqualToSrc.xlf").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out");
		outFile.delete();

		filter.parseFile(inFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertEquals(null, omegat.getTranslation(null, "0"));
		assertEquals("1", omegat.getTranslation(null, "1"));
		assertEquals(null, omegat.getTranslation(null, "2"));
		assertEquals("3", omegat.getTranslation(null, "3"));
		filter.translateFile(inFile, outFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());
		
		assertTrue(Utils.findInFile(outFile, "target xml:lang=\"fr-fr\" state=\":final\">3</target>"));
	}
	
	@Test
	public void test_AllowEmptyTarget () throws Exception {
		XLIFFFilter filter = new XLIFFFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/withEmptyTarget.xlf").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out");
		outFile.delete();
		
		String filterConf = new File(getClass().getResource("/okf_xliff@allowEmptyTargets.fprm").toURI()).getAbsolutePath();
		Map<String, String> options = new HashMap<>();
		options.put(AbstractOkapiFilter.USE_DEFAULT, AbstractOkapiFilter.VALUE_NO);
		options.put(AbstractOkapiFilter.USE_CUSTOM, filterConf);

		filter.parseFile(inFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		omegat.translateSome(1);
		filter.translateFile(inFile, outFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());
		
		assertTrue(Utils.findInFile(outFile, "<target xml:lang=\"fr-fr\">0 kilowatts</target>"));
		assertFalse(Utils.findInFile(outFile, "<target xml:lang=\"fr-fr\">Some other text</target>"));
		assertTrue(Utils.findInFile(outFile, "<target xml:lang=\"fr-fr\">Last sentence</target>"));
	}
	
	@Test
	public void testXLIFFProcess_Notes () throws Exception {
		XLIFFFilter filter = new XLIFFFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/example_v12.xlf").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out");
		outFile.delete();

		filter.parseFile(inFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertEquals("Text of the note", omegat.getEntryComment(0));
		filter.translateFile(inFile, outFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());
		
		assertTrue(Utils.findInFile(outFile, "Text of the note"));
	}
	
	@Test
	public void testNoTransExcludedFinalProtected () throws Exception {
		XLIFFFilter filter = new XLIFFFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/multi-cases.xlf").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out1");
		outFile.delete();

		filter.parseFile(inFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		// Check first extracted: it should be the target text if the first entry with not translate='no'
		// It's the target because it is protected and has a target
		assertEquals("Texte avec state=final", omegat.getEntrySource(0));
		// Now the translatable entries
		assertEquals("Texte avec state=final", omegat.getProtectedParts(0).get(0).getTextInSourceSegment());
		assertEquals("<g1>", omegat.getProtectedParts(1).get(0).getTextInSourceSegment());
		assertEquals("</g1>", omegat.getProtectedParts(1).get(1).getTextInSourceSegment());
		assertEquals("Texte avec des codes et <g2>state=final</g2>", omegat.getProtectedParts(2).get(0).getTextInSourceSegment());
		
		omegat.translate();
		filter.translateFile(inFile, outFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\">FR Not to be translated"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\" state=\"final\">FR Final not to be translated"));
		// No source copy after m32: assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\">Not to be translated no target"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\" state=\"final\">Texte avec state=final"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\">Some text with codes "));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\" state=\"final\">Texte avec des codes et <g id=\"2\">state=final</g>"));
	}

	@Test
	public void testNoTransExcludedFinalNotProtected () throws Exception {
		XLIFFFilter filter = new XLIFFFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/multi-cases.xlf").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out2");
		outFile.delete();

		Map<String, String> options = new HashMap<>();
		options.put(XLIFFFilter.PROTECT_FINAL, XLIFFFilter.VALUE_NO);

		filter.parseFile(inFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		// Check first extracted
		assertEquals("Text with state=final", omegat.getEntrySource(0));
		// Now the translatable entries
		assertEquals(Collections.emptyList(), omegat.getProtectedParts(0));
		assertEquals("<g1>", omegat.getProtectedParts(1).get(0).getTextInSourceSegment());
		assertEquals("</g1>", omegat.getProtectedParts(1).get(1).getTextInSourceSegment());
		assertEquals("<g2>", omegat.getProtectedParts(2).get(0).getTextInSourceSegment());
		assertEquals("</g2>", omegat.getProtectedParts(2).get(1).getTextInSourceSegment());
		
		omegat.translate();
		filter.translateFile(inFile, outFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());
		// Un-protected text was replace by source 
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\">FR Not to be translated"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\" state=\"final\">FR Final not to be translated"));
		// No source copy after m32: assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\">Not to be translated no target"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\" state=\"final\">Text with state=final"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\">Some text with codes <g id=\"1\">and no state</g>"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\" state=\"final\">Text with codes and <g id=\"2\">state=final</g>"));
	}

	@Test
	public void testNoTransProtectedFinalProtected () throws Exception {
		XLIFFFilter filter = new XLIFFFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/multi-cases.xlf").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out2");
		outFile.delete();

		Map<String, String> options = new HashMap<>();
		options.put(XLIFFFilter.PROTECT_NOTRANS, XLIFFFilter.VALUE_YES);
		// This is the default: options.put(XLIFFFilter.PROTECT_FINAL, XLIFFFilter.VALUE_YES);

		filter.parseFile(inFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		// Check non-translatable: should be protected
		assertEquals("FR Not to be translated", omegat.getProtectedParts(0).get(0).getTextInSourceSegment());
		assertEquals("FR Final not to be translated", omegat.getProtectedParts(1).get(0).getTextInSourceSegment());
		assertEquals("Not to be translated no target", omegat.getProtectedParts(2).get(0).getTextInSourceSegment());
		// Now the translatable entries
		assertEquals("Texte avec state=final", omegat.getProtectedParts(3).get(0).getTextInSourceSegment());
		assertEquals("<g1>", omegat.getProtectedParts(4).get(0).getTextInSourceSegment());
		assertEquals("</g1>", omegat.getProtectedParts(4).get(1).getTextInSourceSegment());
		assertEquals("Texte avec des codes et <g2>state=final</g2>", omegat.getProtectedParts(5).get(0).getTextInSourceSegment());
		
		omegat.translate();
		filter.translateFile(inFile, outFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());
		// Un-protected text was replace by source 
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\">FR Not to be translated"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\" state=\"final\">FR Final not to be translated"));
		// No source copy after m32: assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\">Not to be translated no target"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\" state=\"final\">Texte avec state=final"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\">Some text with codes "));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\" state=\"final\">Texte avec des codes et <g id=\"2\">state=final</g>"));
	}

	@Test
	public void testNoTransProtectedFinalNotProtected () throws Exception {
		XLIFFFilter filter = new XLIFFFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/multi-cases.xlf").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out2");
		outFile.delete();

		Map<String, String> options = new HashMap<>();
		options.put(XLIFFFilter.PROTECT_NOTRANS, XLIFFFilter.VALUE_YES);
		options.put(XLIFFFilter.PROTECT_FINAL, XLIFFFilter.VALUE_NO);

		filter.parseFile(inFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		// Check non-translatable: should be protected
		assertEquals("FR Not to be translated", omegat.getProtectedParts(0).get(0).getTextInSourceSegment());
		assertEquals("FR Final not to be translated", omegat.getProtectedParts(1).get(0).getTextInSourceSegment());
		assertEquals("Not to be translated no target", omegat.getProtectedParts(2).get(0).getTextInSourceSegment());
		// Now the translatable entries
		assertEquals(Collections.emptyList(), omegat.getProtectedParts(3));
		assertEquals("<g1>", omegat.getProtectedParts(4).get(0).getTextInSourceSegment());
		assertEquals("</g1>", omegat.getProtectedParts(4).get(1).getTextInSourceSegment());
		assertEquals("<g2>", omegat.getProtectedParts(5).get(0).getTextInSourceSegment());
		assertEquals("</g2>", omegat.getProtectedParts(5).get(1).getTextInSourceSegment());
		
		omegat.translate();
		filter.translateFile(inFile, outFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());
		// Un-protected text was replace by source 
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\">FR Not to be translated"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\" state=\"final\">FR Final not to be translated"));
		// No source copy after m32: assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\">Not to be translated no target"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\" state=\"final\">Text with state=final"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\">Some text with codes <g id=\"1\">and no state</g>"));
		assertTrue(Utils.findInFile(outFile, "xml:lang=\"fr-fr\" state=\"final\">Text with codes and <g id=\"2\">state=final</g>"));
	}

	@Test
	public void testHTML () throws Exception {
		HTMLFilter filter = new HTMLFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/W3CHTMHLTest1.html").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out");
		outFile.delete();

		filter.parseFile(inFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		
		// Check first entry
		assertEquals("Element Traversal Specification", omegat.getEntrySource(0));

		omegat.translateModify();
		filter.translateFile(inFile, outFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());

		// Check after translation (first and last)
		assertTrue(Utils.findInFile(outFile, "<title>TR_Element Traversal Specification</title>"));
		assertTrue(Utils.findInFile(outFile, "<p>TR_The editor would like to thank the"));
	}

	@Test
	public void testXMLStreamFilter () throws Exception {
		XMLStreamFilter filter = new XMLStreamFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/xmlstream-test.xml").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out");
		outFile.delete();

		String filterConf = new File(getClass().getResource("/okf_xmlstream@myf.fprm").toURI()).getAbsolutePath();
		Map<String, String> options = new HashMap<>();
		options.put(AbstractOkapiFilter.USE_DEFAULT, AbstractOkapiFilter.VALUE_NO);
		options.put(AbstractOkapiFilter.USE_CUSTOM, filterConf);
		filter.parseFile(inFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertEquals("text", omegat.getEntrySource(0));
		assertEquals("para 2", omegat.getEntrySource(3));

		filter.translateFile(inFile, outFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());
		
//		assertTrue(Utils.findInFile(outFile, "target xml:lang=\"fr-fr\">   Some data ["));
	}

	@Test
	public void testYAML () throws Exception {
		YAMLFilter filter = new YAMLFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/en.yml").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out");
		outFile.delete();

		filter.parseFile(inFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		
		// Check first entry
		assertEquals("First entry.", omegat.getEntrySource(0));

		omegat.translateModify();
		filter.translateFile(inFile, outFile, null, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());

		// Check after translation (first and last)
		assertTrue(Utils.findInFile(outFile, "applyTagsBulk: TR_First entry."));
		assertTrue(Utils.findInFile(outFile, "alreadyVoted: TR_Last entry."));
	}

	@Test
	public void customJsonFilterWithCustomHtmlSubfilter() throws Exception {
		JSONFilter filter = new JSONFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/json-with-html.json").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out");
		outFile.delete();

		String filterConf = new File(getClass().getResource("/okf_json@with-subfilter.fprm").toURI()).getAbsolutePath();
		Map<String, String> options = new HashMap<>();
		options.put(AbstractOkapiFilter.USE_DEFAULT, AbstractOkapiFilter.VALUE_NO);
		options.put(AbstractOkapiFilter.USE_CUSTOM, filterConf);

		filter.parseFile(inFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertEquals("k1", omegat.getEntrySource(0));
		assertEquals("<g1>Title</g1>", omegat.getEntrySource(1));
		assertEquals("Text", omegat.getEntrySource(2));

		filter.translateFile(inFile, outFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());
		assertTrue(Utils.findInFile(outFile, "  \"title\": \"<div id=\\\"title-k1\\\"><strong>Title<\\/strong><\\/div>\\n\","));
	}

	@Test
	public void protectedPartsReflectCodesOriginalInformation() throws Exception {
		JSONFilter filter = new JSONFilter();
		VirtualOmegaT omegat = new VirtualOmegaT();
		File inFile = new File(getClass().getResource("/43.json").toURI());
		File outFile = new File(inFile.getAbsolutePath()+".out");
		outFile.delete();

		String filterConf = new File(getClass().getResource("/okf_json@43.fprm").toURI()).getAbsolutePath();
		Map<String, String> options = new HashMap<>();
		options.put(AbstractOkapiFilter.USE_DEFAULT, AbstractOkapiFilter.VALUE_NO);
		options.put(AbstractOkapiFilter.USE_CUSTOM, filterConf);

		filter.parseFile(inFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);

		assertEquals("<x1/>", omegat.getProtectedParts(0).get(0).getTextInSourceSegment());
		assertEquals("{diagramCount}", omegat.getProtectedParts(0).get(0).getDetailsFromSourceFile());
		assertEquals("<x2/>", omegat.getProtectedParts(0).get(1).getTextInSourceSegment());
		assertEquals("{diagramNumber}", omegat.getProtectedParts(0).get(1).getDetailsFromSourceFile());

		omegat.translate();
		filter.translateFile(inFile, outFile, options, new FilterContext(new Language("en-us"), new Language("fr-fr"), false), omegat);
		assertTrue(outFile.exists());
		assertTrue(Utils.findInFile(outFile, "\"Text\": \"Diagram ({diagramCount}) and another number {diagramNumber}\""));
	}
}
