package net.sf.okapi.lib.omegat;

import java.util.HashMap;
import java.util.Map;

public class ManualTry {

	public static void main (String [] args) {
		Map<String, String> options = new HashMap<>();

		XLIFFOptions xdlg = new XLIFFOptions(null, options, "okf_xliff");
		xdlg.setVisible(true);
		options = xdlg.getOptions();
		System.out.println("xliff-1:"+options);
		xdlg = new XLIFFOptions(null, options, "okf_xliff");
		xdlg.setVisible(true);
		options = xdlg.getOptions();
		System.out.println("xliff-2:"+options);
		
		options = new HashMap<>();
		// Default: First call
		DefaultOptions dlg = new DefaultOptions(null, options, "okf_ttx");
		dlg.setVisible(true);
		options = dlg.getOptions();
		System.out.println("default-1:"+options);
		// Default: Second call (same options)
		dlg = new DefaultOptions(null, options, "okf_ttx");
		dlg.setVisible(true);
		options = dlg.getOptions();
		System.out.println("default-2:"+options);
	}
}
