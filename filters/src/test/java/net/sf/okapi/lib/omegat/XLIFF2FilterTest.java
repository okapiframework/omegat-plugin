package net.sf.okapi.lib.omegat;

import static org.junit.Assert.assertEquals;

import java.io.StringWriter;

import net.sf.okapi.lib.xliff2.core.CTag;
import net.sf.okapi.lib.xliff2.core.Fragment;
import net.sf.okapi.lib.xliff2.core.MTag;
import net.sf.okapi.lib.xliff2.core.Part;
import net.sf.okapi.lib.xliff2.core.Part.GetTarget;
import net.sf.okapi.lib.xliff2.core.Segment;
import net.sf.okapi.lib.xliff2.core.TagType;
import net.sf.okapi.lib.xliff2.core.Tags;
import net.sf.okapi.lib.xliff2.core.Unit;
import net.sf.okapi.lib.xliff2.reader.Event;
import net.sf.okapi.lib.xliff2.reader.XLIFFReader;
import net.sf.okapi.lib.xliff2.writer.XLIFFWriter;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XLIFF2FilterTest {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	XLIFF2Filter filter;
	
	@Before
	public void setUp() {
		filter = new XLIFF2Filter();
	}

	@Test
	public void testConversionToOmegat () {
		Unit unit = new Unit("1");
		Fragment frag = unit.appendSegment().getSource();
		frag.append(TagType.OPENING, "i1", "<b>", true);
		frag.append(TagType.STANDALONE, "i2", "<br/>", true);
		frag.append(TagType.CLOSING, "i1", "</b>", true);
		frag.append(TagType.OPENING, "i3", "<u>", true);
		frag.append(TagType.CLOSING, "i4", "</i>", true);
		// Test
		assertEquals("<g0><x1/></g0><g2></g3>", filter.toOmegat(unit.getPart(0), false));
	}

	@Test
	public void testConversionToOmegatWithPC () {
		Unit unit = new Unit("1");
		Fragment frag = unit.appendSegment().getSource();
		MTag am = new MTag("m1", MTag.TYPE_DEFAULT);
		am.setTranslate(false);
		frag.append(am);
		frag.append(TagType.OPENING, "i1", "<b>", true);
		frag.append("nn");
		frag.append(TagType.STANDALONE, "i2", "<br/>", true);
		frag.append(TagType.CLOSING, "i1", "</b>", true);
		frag.closeMarkerSpan("m1");
		frag.append("tt");
		frag.append(TagType.OPENING, "i3", "<u>", true);
		frag.append(TagType.CLOSING, "i4", "</i>", true);
		// Test
		unit.hideProtectedContent();
		assertEquals("<p0/>tt<g1></g2>", filter.toOmegat(unit.getPart(0), false));
	}

	@Test
	public void testConversionFromOmegatOnlySource () {
		Unit unit = new Unit("1");
		Fragment frag = unit.appendSegment().getSource();
		frag.append(TagType.OPENING, "g0", "<b>", true);
		frag.append(TagType.STANDALONE, "x1", "<br/>", true);
		frag.append(TagType.CLOSING, "g0", "</b>", true);
		frag.openMarkerSpan("m2", "comment").setValue("my comment");
		frag.append(TagType.OPENING, "g2", "<u>", true);
		frag.append(TagType.CLOSING, "g3", "</i>", true);
		frag.closeMarkerSpan("m2");

		Segment seg = (Segment)unit.getPart(0);
		String omt = filter.toOmegat(seg, false);
		assertEquals("<c1(g0)><c2(x1)/></c3(g0)><a4(m2)><c5(g2)></c6(g3)></a7(m2)>",
			showCT(null, seg.getSource().getCodedText(), seg.getSourceTags()));
		filter.fromOmegat(omt, seg, false);
//		assertEquals("<c1(g0)><c2(g1)/></c3(g0)><a4(m2)><c5(g3)></c6(g4)></a7(m2)>",
//			showCT(null, seg.getTarget().getCodedText(), seg.getTargetTags()));
		assertEquals("<c1(g0)><c2(x1)/></c3(g0)><c4(g2)></c5(g3)>",
			showCT(null, seg.getTarget().getCodedText(), seg.getTargetTags()));
		
		Tags m = seg.getTarget().getTags();
		assertEquals(5, m.size());
		// But order of the code and index may differ (which should be fine)
		CTag cm = (CTag)m.get("g0", TagType.OPENING);
		assertEquals("<b>", cm.getData());
		cm = (CTag)m.get("x1", TagType.STANDALONE);
		assertEquals("<br/>", cm.getData());
		cm = (CTag)m.get("g0", TagType.CLOSING);
		assertEquals("</b>", cm.getData());
		cm = (CTag)m.get("g2", TagType.OPENING);
		assertEquals("<u>", cm.getData());
		cm = (CTag)m.get("g3", TagType.CLOSING);
		assertEquals("</i>", cm.getData());
	}

	@Test
	public void testConversionFromOmegatWithTarget () {
		Unit unit = new Unit("1");
		Fragment frag = unit.appendSegment().getSource();
		frag.append(TagType.OPENING, "g0", "<b>", true);
		frag.append(TagType.STANDALONE, "x1", "<br/>", true);
		frag.append(TagType.CLOSING, "g0", "</b>", true);
		MTag am = frag.openMarkerSpan("m2", "comment");
		am.setValue("my comment");
		am.setTranslate(false);
		frag.append(TagType.OPENING, "g3", "<u>", true);
		frag.append(TagType.CLOSING, "g4", "</i>", true);
		frag.closeMarkerSpan("m2");
		frag.append("srctext");
		// Create the target (but without annotation)
		frag = unit.getPart(0).getTarget(GetTarget.CREATE_EMPTY);
		frag.append(TagType.OPENING, "g0", "<b>", true);
		frag.append(TagType.CLOSING, "g0", "</b>", true);
		frag.append(TagType.OPENING, "g3", "<u>", true);
		frag.append(TagType.STANDALONE, "x1", "<br/>", true); // Moved compared to source
		frag.append(TagType.CLOSING, "g4", "</i>", true);
		frag.append(TagType.STANDALONE, "new", "<new/>", true);
		frag.append("trgtext");

		unit.hideProtectedContent();
		Segment seg = (Segment)unit.getPart(0);
		assertEquals("<c1(g0)><c2(x1)/></c3(g0)><p4/>srctext",
			showCT(null, seg.getSource().getCodedText(), seg.getSourceTags()));
		assertEquals("<c1(g0)></c2(g0)><c3(g3)><c4(x1)/></c5(g4)><c6(new)/>trgtext",
			showCT(null, seg.getTarget().getCodedText(), seg.getTargetTags()));
		
		String omt = filter.toOmegat(seg, true);
		filter.fromOmegat(omt, seg, true);
		assertEquals("<c1(g0)></c2(g0)><c3(g3)><c4(x1)/></c5(g4)><c6(4)/>trgtext", // <c6(4)/> is the extra marker
			showCT(null, seg.getTarget().getCodedText(), seg.getTargetTags()));

		unit.showProtectedContent();
		
		Tags m = seg.getTarget().getTags();
		assertEquals(6, m.size());
		// But order of the code and index may differ (which should be fine)
		CTag cm = (CTag)m.get("g0", TagType.OPENING);
		assertEquals("<b>", cm.getData());
		cm = (CTag)m.get("x1", TagType.STANDALONE);
		assertEquals("<br/>", cm.getData());
		cm = (CTag)m.get("g0", TagType.CLOSING);
		assertEquals("</b>", cm.getData());
		cm = (CTag)m.get("g3", TagType.OPENING);
		assertEquals("<u>", cm.getData());
		cm = (CTag)m.get("g4", TagType.CLOSING);
		assertEquals("</i>", cm.getData());
	}

	@Test
	public void testConversionFromOmegatWithTwoTargets () {
		Unit unit = new Unit("1");
		// Segment 1
		Fragment frag = unit.appendSegment().getSource();
		frag.append(TagType.OPENING, "g0", "<b>", true);
		frag.append(TagType.STANDALONE, "x1", "<br/>", true);
		frag.append(TagType.CLOSING, "g0", "</b>", true);
		frag.append("src. ");
		frag = unit.getPart(0).getTarget(GetTarget.CREATE_EMPTY);
		frag.append(TagType.OPENING, "g0", "<b>", true);
		frag.append(TagType.CLOSING, "g0", "</b>", true);
		frag.append(TagType.STANDALONE, "x1", "<br/>", true); // Moved compared to source
		frag.append("trg. ");
		// Segment 2
		frag = unit.appendSegment().getSource();
		frag.append(TagType.OPENING, "g1", "<b>", true);
		frag.append(TagType.CLOSING, "g1", "</b>", true);
		frag.append("src2.");
		frag = unit.getPart(1).getTarget(GetTarget.CREATE_EMPTY);
		frag.append(TagType.OPENING, "g1", "<b>", true);
		frag.append(TagType.CLOSING, "g1", "</b>", true);
		frag.append("trg2.");

		unit.hideProtectedContent();
		Segment seg = (Segment)unit.getPart(0);
		String omt = filter.toOmegat(seg, true);
		filter.fromOmegat(omt, seg, true);
		seg = (Segment)unit.getPart(1);
		omt = filter.toOmegat(seg, true);
		filter.fromOmegat(omt, seg, true);
		unit.showProtectedContent();

		assertEquals("<c1(g0)></c2(g0)><c3(x1)/>trg. ",
			showCT(null, unit.getPart(0).getTarget().getCodedText(), unit.getPart(0).getTargetTags()));
		unit.getPart(0).getTarget().toXLIFF();
		assertEquals("<c1(g1)></c2(g1)>trg2.",
			showCT(null, unit.getPart(1).getTarget().getCodedText(), unit.getPart(1).getTargetTags()));
		unit.getPart(1).getTarget().toXLIFF();
		
	}

//TODO: Fix	@Test
	public void testRoundTripSimple () {
		String snippet = "<?xml version=\"1.0\"?>\n"
			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en-us\">\n"
			+ "<file id=\"f1\">\n"
			+ "<skeleton href=\"skeleton.skl\"></skeleton>"
			+ "<unit id=\"1\">\n"
			+ "<segment>\n"
			+ "<source><pc id=\"c1\"><mrk id=\"m1\" type=\"term\">Text1</mrk>.<ph id=\"c2\"/> </pc></source>\n"
			+ "</segment>\n"
			+ "<ignorable>\n<source> data<ph id=\"c3\"/></source>\n</ignorable>\n"
			+ "</unit>\n</file>\n</xliff>\n";
		String result = doRoundTrip(snippet);
		String expected = "<?xml version=\"1.0\"?>\n"
			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en-us\" trgLang=\"fr\">\n"
			+ "<file id=\"f1\">\n"
			+ "<skeleton href=\"skeleton.skl\"></skeleton>\n"
			+ "<unit id=\"1\">\n"
			+ "<segment>\n"
			+ "<source><pc id=\"c1\"><mrk id=\"m1\" type=\"term\">Text1</mrk>.<ph id=\"c2\"/> </pc></source>\n"
			+ "<target><pc id=\"c1\"><mrk id=\"m1\" type=\"term\">Text1</mrk>.<ph id=\"c2\"/> </pc></target>\n"
			+ "</segment>\n"
			+ "<ignorable>\n<source> data<ph id=\"c3\"/></source>\n<target> data<ph id=\"c3\"/></target>\n</ignorable>\n"
			+ "</unit>\n</file>\n</xliff>\n";
		assertEquals(expected, result);
	}

	@Test
	public void testRoundTripSimpleWithTarget () {
		String snippet = "<?xml version=\"1.0\"?>\n"
			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en-us\">\n"
			+ "<file id=\"f1\">\n"
			+ "<skeleton href=\"skeleton.skl\"></skeleton>"
			+ "<unit id=\"1\">\n"
			+ "<segment>\n"
			+ "<source><pc id=\"c1\"><mrk id=\"m1\" type=\"term\">Text1</mrk>.<ph id=\"c2\"/> </pc></source>\n"
			+ "<target><pc id=\"c1\"><ph id=\"c2\"/><mrk id=\"m1\" type=\"term\">TEXT1</mrk> ETC. </pc></target>\n"
			+ "</segment>\n"
			+ "<segment>\n"
			+ "<source><pc id=\"c3\"><mrk id=\"21\" type=\"term\">Text2</mrk>.</pc></source>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n";
		String result = doRoundTrip(snippet);
//		String expected = "<?xml version=\"1.0\"?>\n"
//			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en-us\" trgLang=\"fr\">\n"
//			+ "<file id=\"f1\">\n"
//			+ "<skeleton href=\"skeleton.skl\"></skeleton>\n"
//			+ "<unit id=\"1\">\n"
//			+ "<segment>\n"
//			+ "<source><pc id=\"c1\"><mrk id=\"m1\" type=\"term\">Text1</mrk>.<ph id=\"c2\"/> </pc></source>\n"
//			+ "<target><pc id=\"c1\"><ph id=\"c2\"/><mrk id=\"m1\" type=\"term\">TEXT1</mrk> ETC. </pc></target>\n"
//			+ "</segment>\n"
//			+ "<segment>\n"
//			+ "<source><pc id=\"c3\"><mrk id=\"21\" type=\"term\">Text2</mrk>.</pc></source>\n"
//			+ "<target><pc id=\"c3\"><mrk id=\"21\" type=\"term\">Text2</mrk>.</pc></target>\n"
//			+ "</segment>\n"
//			+ "</unit>\n</file>\n</xliff>\n";
		String expected = "<?xml version=\"1.0\"?>\n"
			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en-us\" trgLang=\"fr\">\n"
			+ "<file id=\"f1\">\n"
			+ "<skeleton href=\"skeleton.skl\"></skeleton>\n"
			+ "<unit id=\"1\">\n"
			+ "<segment>\n"
			+ "<source><pc id=\"c1\"><mrk id=\"m1\" type=\"term\">Text1</mrk>.<ph id=\"c2\"/> </pc></source>\n"
			+ "<target><pc id=\"c1\"><ph id=\"c2\"/>TEXT1 ETC. </pc></target>\n"
			+ "</segment>\n"
			+ "<segment>\n"
			+ "<source><pc id=\"c3\"><mrk id=\"21\" type=\"term\">Text2</mrk>.</pc></source>\n"
			+ "<target><pc id=\"c3\">Text2.</pc></target>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n";
		assertEquals(expected, result);
	}
	
	@Test
	public void testRoundTripWithScEc () {
		String snippet = "<?xml version=\"1.0\"?>\n"
			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en-us\">\n"
			+ "<file id=\"f1\">\n"
			+ "<skeleton href=\"skeleton.skl\"></skeleton>"
			+ "<unit id=\"1\">\n"
			+ "<segment>\n"
			+ "<source><sc id=\"c1\"/><mrk id=\"m1\" type=\"term\">Text1</mrk>. </source>\n"
			+ "</segment>\n"
			+ "<segment>\n"
			+ "<source>Text1<ec startRef=\"c1\"/>.<ph id=\"c2\"/> </source>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n";
		String result = doRoundTrip(snippet);
//		String expected = "<?xml version=\"1.0\"?>\n"
//			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en-us\" trgLang=\"fr\">\n"
//			+ "<file id=\"f1\">\n"
//			+ "<skeleton href=\"skeleton.skl\"></skeleton>\n"
//			+ "<unit id=\"1\">\n"
//			+ "<segment>\n"
//			+ "<source><sc id=\"c1\"/><mrk id=\"m1\" type=\"term\">Text1</mrk>. </source>\n"
//			+ "<target><sc id=\"c1\"/><mrk id=\"m1\" type=\"term\">Text1</mrk>. </target>\n"
//			+ "</segment>\n"
//			+ "<segment>\n"
//			+ "<source>Text1<ec startRef=\"c1\"/>.<ph id=\"c2\"/> </source>\n"
//			+ "<target>Text1<ec startRef=\"c1\"/>.<ph id=\"c2\"/> </target>\n"
//			+ "</segment>\n"
//			+ "</unit>\n</file>\n</xliff>\n";
		String expected = "<?xml version=\"1.0\"?>\n"
			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en-us\" trgLang=\"fr\">\n"
			+ "<file id=\"f1\">\n"
			+ "<skeleton href=\"skeleton.skl\"></skeleton>\n"
			+ "<unit id=\"1\">\n"
			+ "<segment>\n"
			+ "<source><sc id=\"c1\"/><mrk id=\"m1\" type=\"term\">Text1</mrk>. </source>\n"
			+ "<target><sc id=\"c1\"/>Text1. </target>\n"
			+ "</segment>\n"
			+ "<segment>\n"
			+ "<source>Text1<ec startRef=\"c1\"/>.<ph id=\"c2\"/> </source>\n"
			+ "<target>Text1<ec startRef=\"c1\"/>.<ph id=\"c2\"/> </target>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n";
		assertEquals(expected, result);
	}
	
//TODO: Fix	@Test
	public void testRoundTripWithTranslateNo () {
		String snippet = "<?xml version=\"1.0\"?>\n"
			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en-us\">\n"
			+ "<file id=\"f1\">\n"
			+ "<skeleton href=\"skeleton.skl\"></skeleton>"
			+ "<unit id=\"1\">\n"
			+ "<segment>\n"
			+ "<source><pc id=\"c1\"><mrk id=\"m1\" type=\"term\">Text1</mrk>. <mrk id=\"m2\" translate=\"no\"><ph id=\"c2\"/>Data. </mrk></pc></source>\n"
			+ "</segment>\n"
			+ "<ignorable>\n"
			+ "<source> <sm id=\"m3\" translate=\"no\"/></source>\n"
			+ "</ignorable>\n"
			+ "<segment>\n"
			+ "<source>trans<em startRef=\"m3\"/></source>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n";
		String result = doRoundTrip(snippet);
		String expected = "<?xml version=\"1.0\"?>\n"
			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en-us\" trgLang=\"fr\">\n"
			+ "<file id=\"f1\">\n"
			+ "<skeleton href=\"skeleton.skl\"></skeleton>\n"
			+ "<unit id=\"1\">\n"
			+ "<segment>\n"
			+ "<source><pc id=\"c1\"><mrk id=\"m1\" type=\"term\">Text1</mrk>. <mrk id=\"m2\" translate=\"no\"><ph id=\"c2\"/>Data. </mrk></pc></source>\n"
			+ "<target><pc id=\"c1\">Text1. <mrk id=\"m2\" translate=\"no\"><ph id=\"c2\"/>Data. </mrk></pc></target>\n"
			+ "</segment>\n"
			+ "<ignorable>\n"
			+ "<source> <sm id=\"m3\" translate=\"no\"/></source>\n"
			+ "<target> <sm id=\"m3\" translate=\"no\"/></target>\n"
			+ "</ignorable>\n"
			+ "<segment>\n"
			+ "<source>trans<em startRef=\"m3\"/></source>\n"
			+ "<target>trans<em startRef=\"m3\"/></target>\n"
			+ "</segment>\n"
			+ "</unit>\n</file>\n</xliff>\n";
		assertEquals(expected, result);
	}
	
//	@Test
//	public void testRoundTripComplex () {
//		String snippet = "<?xml version=\"1.0\"?>\n"
//			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en-us\">\n"
//			+ "<file id=\"f1\">\n"
//			+ "<skeleton href=\"skeleton.skl\"></skeleton>"
//			+ "<unit id=\"1\">\n"
//			+ "<segment>\n"
//			+ "<source><sc id=\"1\"/><mrk id=\"m1\" type=\"term\">Text1</mrk>. </source>\n"
//			+ "<target><sc id=\"1\" isolated='yes'/>TEXT1. </target>\n"
//			+ "</segment>\n"
//			+ "<segment>\n"
//			+ "<source>Text2.<ec startRef=\"1\"/> </source>\n"
//			+ "</segment>\n"
//			+ "<segment>\n"
//			+ "<source><ph id=\"2\"/>text3 <mrk id=\"m2\" translate=\"no\">data <ph id=\"3\"/></mrk> <ph id=\"4\"/> end.</source>\n"
//			+ "<target><ph id=\"2\"/>TEXT3 <mrk id=\"m2\" translate=\"no\">data <ph id=\"3\"/></mrk> <ph id=\"4\"/> END.</target>\n"
//			+ "</segment>\n"
//			+ "</unit>\n</file>\n</xliff>\n";
//		String result = doRoundTrip(snippet);
//		String expected = "<?xml version=\"1.0\"?>\n"
//			+ "<xliff xmlns=\"urn:oasis:names:tc:xliff:document:2.0\" version=\"2.0\" srcLang=\"en-us\" trgLang=\"fr\">\n"
//			+ "<file id=\"f1\">\n"
//			+ "<skeleton href=\"skeleton.skl\"></skeleton>\n"
//			+ "<unit id=\"1\">\n"
//			+ "<segment>\n"
//			+ "<source><sc id=\"1\"/><mrk id=\"m1\" type=\"term\">Text1</mrk>. </source>\n"
//			+ "<target><sc id=\"1\"/>TEXT1. </target>\n"
//			+ "</segment>\n"
//			+ "<segment>\n"
//			+ "<source>Text2.<ec startRef=\"1\"/> </source>\n"
//			+ "<target>Text2.<ec startRef=\"1\"/> </target>\n"
//			+ "</segment>\n"
//			+ "<segment>\n"
//			+ "<source><ph id=\"2\"/>text3 <mrk id=\"m2\" translate=\"no\">data <ph id=\"3\"/></mrk> <ph id=\"4\"/> end.</source>\n"
//			+ "<target><ph id=\"2\"/>TEXT3 <mrk id=\"m2\" translate=\"no\">data <ph id=\"3\"/></mrk> <ph id=\"4\"/> END.</target>\n"
//			+ "</segment>\n"
//			+ "</unit>\n</file>\n</xliff>\n";
//
//		assertEquals(expected, result);
//	}
	
	private String doRoundTrip (String snippet) {
		XLIFFReader reader = new XLIFFReader();
		reader.open(snippet);
		XLIFFWriter writer = new XLIFFWriter();
		writer.setLineBreak("\n");
		StringWriter sw = new StringWriter();
		writer.create(sw, "en-us", "fr"); // Target is added
		
		while ( reader.hasNext() ) {
			Event event = reader.next();
			if ( event.isUnit() ) {
				Unit unit = event.getUnit();
				//unit.collapseProtectedContent();
				for ( Segment seg : unit.getSegments() ) {
					showCT("src before", seg.getSource().getCodedText(), seg.getSourceTags());
					if ( seg.hasTarget() ) showCT("trg before", seg.getTarget().getCodedText(), seg.getTargetTags());
					if ( seg.hasTarget() ) {
						String omt = filter.toOmegat(seg, true);
						filter.fromOmegat(omt, seg, true);
						showCT("trg -after", seg.getTarget().getCodedText(), seg.getTargetTags());
					}
					else {
						String omt = filter.toOmegat(seg, false);
						filter.fromOmegat(omt, seg, false);
					}
				}
				unit.showProtectedContent();
				for ( Part part : unit ) {
					showCT("src part", part.getSource().getCodedText(), part.getSourceTags());
					showCT("trg part", part.getTarget().getCodedText(), part.getTargetTags());
				}
			}
			writer.writeEvent(event);
		}
		writer.close();
		reader.close();

		String result = sw.toString();
		XLIFFReader.validate(result, null);
		return result;
	}

	private String showCT (String msg,
		String ct,
		Tags tags)
	{
		StringBuilder tmp = new StringBuilder();
		int index = 0;
		for ( int i=0; i<ct.length(); i++ ) {
			switch ( ct.charAt(i) ) {
			case Fragment.CODE_OPENING:
				index++;
				tmp.append(String.format("<c%d(%s)>", index, tags.get(ct, i).getId())); i++;
				break;
			case Fragment.CODE_CLOSING:
				index++;
				tmp.append(String.format("</c%d(%s)>", index, tags.get(ct, i).getId())); i++;
				break;
			case Fragment.CODE_STANDALONE:
				index++;
				tmp.append(String.format("<c%d(%s)/>", index, tags.get(ct, i).getId())); i++;
				break;
			case Fragment.MARKER_OPENING:
				index++;
				tmp.append(String.format("<a%d(%s)>", index, tags.get(ct, i).getId())); i++;
				break;
			case Fragment.MARKER_CLOSING:
				index++;
				tmp.append(String.format("</a%d(%s)>", index, tags.get(ct, i).getId())); i++;
				break;
			case Fragment.PCONT_STANDALONE:
				index++;
				tmp.append(String.format("<p%d/>", index)); i++;
				break;
			default:
				tmp.append(ct.charAt(i));
				break;
			}
		}
		
		if ( msg != null ) 
			LOGGER.trace("{}: {}", msg, tmp.toString());
		return tmp.toString();
	}
}
