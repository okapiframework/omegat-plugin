package net.sf.okapi.lib.omegat;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.omegat.core.data.ProtectedPart;
import org.omegat.filters2.IFilter;
import org.omegat.filters2.IParseCallback;
import org.omegat.filters2.ITranslateCallback;

public class VirtualOmegaT implements IParseCallback, ITranslateCallback {

	private class Item {
		
//		public String id;
		public String source;
		public String target;
//		public boolean isFuzzy;
		public String comment;
//		public String path;
		public List<ProtectedPart> protectedParts;
		
		public Item (String id,
			String source,
			String target,
			boolean isFuzzy,
			String comment,
			String path,
			List<ProtectedPart> protectedParts)
		{
//			this.id = id;
			this.source = source;
			this.target = target;
//			this.isFuzzy = isFuzzy;
			this.comment = comment;
//			this.path = path;
			this.protectedParts = protectedParts;
		}
	}
	
	private Map<String, Item> items = new LinkedHashMap<>();

	@Override
    public void addEntryWithProperties(String id, String source, String translation,
                            boolean isFuzzy, String[] props, String path, IFilter filter,
                            List<ProtectedPart> protectedParts) {
		String comment=null;
		if (props.length > 0) {
			comment = props[0];
		}
		Item item = new Item(id, source, translation, isFuzzy, comment, path, protectedParts);
		items.put(id, item);
	}

	@Override
	public void addEntry(String id,
		String source,
		String translation,
		boolean isFuzzy,
		String comment,
		String path,
        IFilter filter,
        List<ProtectedPart> protectedParts)
	{
		Item item = new Item(id, source, translation, isFuzzy, comment, path, protectedParts);
		items.put(id, item);
	}
	
	@Override
	public void addEntry (String id,
		String source,
		String translation,
		boolean isFuzzy,
		String comment,
		IFilter filter)
	{
		Item item = new Item(id, source, translation, isFuzzy, comment, null, null);
		items.put(id, item);
	}

	@Override
	public void linkPrevNextSegments () {
		// TODO Auto-generated method stub
	}

	@Override
	public void setPass (int pass) {
		// TODO Auto-generated method stub
	}

	@Override
	public String getTranslation (String id,
		String source,
		String path)
	{
		Item item = items.get(id);
		if ( item == null ) return null;
		else return item.target;
	}

	@Override
	public String getTranslation (String id,
		String source)
	{
		for ( String itemId : items.keySet() ) {
			Item item = items.get(itemId);
			if ( item.source.equals(source) ) {
				return item.target;
			}
		}
		return null;
	}

	public void translate () {
		for ( Item item : items.values() ) {
			item.target = item.source;
		}
	}
	
	public void translateModify () {
		for ( Item item : items.values() ) {
			item.target = "TR_"+item.source;
		}
	}
	
	/**
	 * Translates the items, expect one at the given index.
	 * @param toSkip the index of the item to not translate.
	 */
	public void translateSome (int toSkip) {
		int i=-1;
		for ( Item item : items.values() ) {
			if ( (++i) == toSkip ) continue;
			item.target = item.source;
		}
	}
	
	public String getEntrySource (int zeroBasedindex) {
		int count = 0;
		for ( Item item : items.values() ) {
			if ( count == zeroBasedindex ) return item.source;
			count++;
		}
		return null;
	}

	public String getEntryComment (int zeroBasedindex) {
		int count = 0;
		for ( Item item : items.values() ) {
			if ( count == zeroBasedindex ) return item.comment;
			count++;
		}
		return null;
	}

	public List<ProtectedPart> getProtectedParts (int zeroBasedindex) {
		int count = 0;
		for ( Item item : items.values() ) {
			if ( count == zeroBasedindex ) return item.protectedParts;
			count++;
		}
		return null;
	}
}
