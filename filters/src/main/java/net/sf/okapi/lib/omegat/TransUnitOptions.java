/*===========================================================================
  Copyright (C) 2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.omegat;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

public class TransUnitOptions extends JDialog {
	
	protected static final ResourceBundle res = ResourceBundle.getBundle("omegat-okapi-plugin", Locale.getDefault());
	
	private static final long serialVersionUID = 1L;
	
	private final Map<String, String> options;
	final JCheckBox chkIncludeTransUnitName;
	
	// Internal class for the option actions
	class OptionsListener implements ActionListener {
		@Override
		public void actionPerformed (ActionEvent event) {}
	}
	
	/**
	 * Creates a TransUnitOptions object.
	 * @param parent the parent dialog.
	 * @param paramOptions the options.
	 */
	public TransUnitOptions (final Window parent,
		final Map<String, String> paramOptions,
		final String defaultConfigId)
	{
		super(parent, res.getString("xliffOptions.window.title"));
		setModal(true);
		
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		if ( paramOptions == null ) {
			this.options = new HashMap<String, String>();
		}
		else {
			this.options = paramOptions;
		}

		Container cp = getContentPane();
	    cp.setLayout(new GridBagLayout());

	    OptionsListener optListner = new OptionsListener();
	    
    	final JPanel pnlOT = new JPanel(new GridBagLayout());

    	chkIncludeTransUnitName = new JCheckBox(res.getString("checkbox.IncludeTransUnitName"));
    	GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0; c.gridwidth = 1;
        c.gridy = 0;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(10, 10, 0, 10);
    	pnlOT.add(chkIncludeTransUnitName, c);
    	
	    c = new GridBagConstraints();
	    c.anchor = GridBagConstraints.LINE_START;
	    c.gridx = 0; c.gridwidth = 3;
	    c.gridy = 3;
	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.weightx = 1.0;
	    c.insets = new Insets(0, 0, 20, 0);
        cp.add(pnlOT, c);

        String val = options.get(AbstractOkapiFilter.INCLUDE_TU_NAME);
        chkIncludeTransUnitName.setSelected(val==null || val.equals(AbstractOkapiFilter.VALUE_YES));
        
    	// Do the actions for the options
    	optListner.actionPerformed(null);

    	final JPanel pnlAction = new JPanel(new GridLayout(1, 2, 5, 0));
	    
	    final JButton btOK = new JButton(res.getString("dialogButtons.okButton"));
	    btOK.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {

    			// OmegaT behavior
   				paramOptions.put(AbstractOkapiFilter.INCLUDE_TU_NAME,
					chkIncludeTransUnitName.isSelected() ? AbstractOkapiFilter.VALUE_YES : AbstractOkapiFilter.VALUE_NO);
   				
   				dispose();
	    	}
	    });
	    pnlAction.add(btOK);
        getRootPane().setDefaultButton(btOK);

        @SuppressWarnings("serial")
        AbstractAction cancelAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        };
        // Allow canceling the dialog with the Esc key
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                "ESC_CLOSE");
        getRootPane().getActionMap().put("ESC_CLOSE", cancelAction);
        
    	final JButton btCancel = new JButton(res.getString("dialogButtons.cancelButton"));
	    btCancel.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		dispose();
	    	}
	    });
	    pnlAction.add(btCancel);
	    
	    c = new GridBagConstraints();
	    c.anchor = GridBagConstraints.LINE_END;
	    c.gridx = 0; c.gridwidth = 3;
	    c.gridy = 4;
	    c.insets = new Insets(0, 10, 10, 10);
	    cp.add(pnlAction, c);

	    pack();
	    setMinimumSize(new Dimension(600, getSize().height));
	    setMaximumSize(new Dimension(600, getSize().height));

        // Center the dialog
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((dim.width-getSize().width)/2, (dim.height-getSize().height)/2);
        
	}
	
	/**
	 * Gets the options.
	 * @return a map of option key-value pairs, never null.
	 */
	public Map<String, String> getOptions () {
		return options;
	}

}
