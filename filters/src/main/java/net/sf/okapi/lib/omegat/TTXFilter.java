/*===========================================================================
  Copyright (C) 2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.omegat;

import java.util.Locale;
import java.util.ResourceBundle;

import org.omegat.filters2.Instance;

public class TTXFilter extends AbstractOkapiFilter {

	private static final String EXTENSION = ".ttx";
	
	protected static final ResourceBundle res = ResourceBundle.getBundle("omegat-okapi-plugin", Locale.getDefault());
	
	public TTXFilter () {
		initialize(res.getString("ttxFilter.name"),
			"okf_ttx",
			EXTENSION);
	}

	@Override
	public Instance[] getDefaultInstances () {
        return new Instance[] {
        	new Instance("*"+EXTENSION)
        };
	}

    @Override
    public boolean isBilingual() {
        return true;
    }
}
