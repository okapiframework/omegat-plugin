/*===========================================================================
  Copyright (C) 2014-2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.omegat;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class XLIFFDetector {

	protected static final ResourceBundle res = ResourceBundle.getBundle("omegat-okapi-plugin", Locale.getDefault());

	/**
	 * Gets the version of an XLIFF document.
	 * @param inputFile the file to inspect.
	 * @return the version of the xliff element.
	 * @throws XMLStreamException if an XML reading error occurred.
	 * @throws IOException if no version could be detected or an error occurred.
	 */
	public static String getXLIFFVersion (File inputFile)
		throws XMLStreamException, IOException
	{
		XMLStreamReader reader = null;
		XMLInputFactory factory = XMLInputFactory.newFactory();
		try {
			reader = factory.createXMLStreamReader(new FileInputStream(inputFile));
			while ( reader.hasNext() ) {
				switch ( reader.next() ) {
				case XMLStreamConstants.START_ELEMENT:
					if ( reader.getName().getLocalPart().equals("xliff") ) {
						String version = reader.getAttributeValue("", "version");
						if ( version == null ) {
							throw new IOException(res.getString("xliffDetector.exception.version"));
						}
						return version;
					}
					// Else: not an XLIFF document
					throw new IOException(res.getString("xliffDetector.exception.noXLIFFElement"));
				}
			}
		}
		finally {
			if ( reader != null ) reader.close();
		}
		
		// Should not get here
		throw new IOException(res.getString("xliffDetector.exception.noElementInFile"));
	}

}
