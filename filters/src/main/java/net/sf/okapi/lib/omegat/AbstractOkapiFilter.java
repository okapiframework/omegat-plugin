/*===========================================================================
  Copyright (C) 2010-2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.omegat;

import java.awt.Dialog;
import java.awt.Window;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import net.sf.okapi.common.encoder.EncoderContext;
import net.sf.okapi.common.resource.CodeMatches;
import net.sf.okapi.common.resource.TextFragmentUtil;
import org.omegat.core.Core;
import org.omegat.core.data.ProtectedPart;
import org.omegat.filters2.FilterContext;
import org.omegat.filters2.IAlignCallback;
import org.omegat.filters2.IParseCallback;
import org.omegat.filters2.ITranslateCallback;
import org.omegat.filters2.TranslationException;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IResource;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.annotation.AltTranslation;
import net.sf.okapi.common.annotation.AltTranslationsAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotation;
import net.sf.okapi.common.annotation.GenericAnnotationType;
import net.sf.okapi.common.annotation.GenericAnnotations;
import net.sf.okapi.common.annotation.Note;
import net.sf.okapi.common.annotation.NoteAnnotation;
import net.sf.okapi.common.filters.DefaultFilters;
import net.sf.okapi.common.filters.FilterConfigurationMapper;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.filterwriter.GenericContent;
import net.sf.okapi.common.filterwriter.IFilterWriter;
import net.sf.okapi.common.filterwriter.TMXWriter;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.ISegments;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.Property;
import net.sf.okapi.common.resource.RawDocument;
import net.sf.okapi.common.resource.Segment;
import net.sf.okapi.common.resource.StartDocument;
import net.sf.okapi.common.resource.TextContainer;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextFragment.CompareMode;
import net.sf.okapi.common.resource.TextFragment.TagType;


abstract class AbstractOkapiFilter implements org.omegat.filters2.IFilter {

	protected static final String INCLUDE_TU_NAME = "includeTUName";
	protected static final String PROTECT_FINAL = "protectFinal";
	protected static final String PROTECT_NOTRANS = "protectNoTrans";
	protected static final String USE_NAME_AS_ID = "useNameAsId";

	protected static final String USE_DEFAULT = "useDefault";
	protected static final String VALUE_YES = "yes";
	protected static final String VALUE_NO = "no";
	protected static final String USE_CUSTOM = "useCustom";
	
	protected static final String OMEGAT_CONFIG_FOLDER = "omegat";

	protected IParseCallback parseCallback;
    protected ITranslateCallback translateCallback;
    protected IAlignCallback alignCallback;
    protected String defaultInEncoding = "UTF-8";
    protected String defaultOutEncoding = "UTF-8";
    protected String supportedExtensions;
	protected String filterConfigId;

    private Boolean processOmegaT2_5 = null;
    private boolean version3_plus = false;

	private FilterConfigurationMapper fcMapper;
	private String name;
	private PrintWriter glosWriter = null;
	private boolean triedGlossaryCreation = false;
	private TMXWriter tmxWriter = null;
	private boolean triedTmxCreation = false;
	private TMXWriter tmxMtWriter = null;
	private boolean triedTmxMtCreation = false;
	private File inputFile;
	private String lastInEncoding = null;

	private static final Logger LOGGER = Logger.getLogger(AbstractOkapiFilter.class.getName());
	
	protected void initialize (String name,
		String filterConfigId,
		String supportedExtensions)
	{
		this.name = name;
		this.filterConfigId = filterConfigId;
		this.supportedExtensions = supportedExtensions;
        // set xpath node limits to unlimited is not specified.
        // recent JDK releases (4/2022) set a hard limit for increased security
		if (System.getProperty("jdk.xml.xpathExprOpLimit") == null) {
			System.setProperty("jdk.xml.xpathExprGrpLimit", "0");
			System.setProperty("jdk.xml.xpathExprOpLimit", "0");
			System.setProperty("jdk.xml.xpathTotalOpLimit", "0");
		}
		fcMapper = new FilterConfigurationMapper();
		// By default do pre-load for sub-filters
		DefaultFilters.setMappings(fcMapper, false, true);
	}

	@Override
	public void alignFile(File inFile,
		File outFile,
		Map<String, String> config,
		FilterContext context,
		IAlignCallback callback)
		throws Exception
    {
		// Do nothing
	}

	@Override
	public Map<String, String> changeOptions (Dialog parent,
		Map<String, String> config)
	{
        return changeOptions((Window) parent, config);
	}
	
	@Override
	public Map<String, String> changeOptions (Window parent,
        Map<String, String> config)
	{
        DefaultOptions dlg = new DefaultOptions(parent, config, filterConfigId);
        dlg.setVisible(true);
        return dlg.getOptions();
	}

	@Override
	public String getFileFormatName () {
		return name;
	}

	@Override
	public String getFuzzyMark () {
		return "fuzzy";
	}

	@Override
	public String getHint () {
		return "";
	}

	@Override
	public boolean hasOptions () {
		return true;
	}

	@Override
	public boolean isFileSupported (File inFile,
		Map<String, String> config,
		FilterContext context)
	{
		return true;
		// Extension check is already done by OmegaT
		// Here we just need to check cases when an extension could be used for
		// several different filters (like .xml)
	}

	@Override
	public boolean isSourceEncodingVariable () {
		return false;
	}

	@Override
	public boolean isTargetEncodingVariable () {
		return true;
	}

	@Override
	public void parseFile (File inFile,
		Map<String, String> config,
		FilterContext context,
		IParseCallback callback)
		throws Exception
	{
		parseCallback = callback;
		translateCallback = null;
		alignCallback = null;
		checkOmegaTVersion();
        try {
            processFile(inFile, null, config, context);

            // 2.5 version also needs to link previous/next
            if ( requirePrevNextFields() ) {
                // parsing - need to link prev/next
                parseCallback.linkPrevNextSegments();
            }
            
        }
        finally {
        	parseCallback = null;
        }
	}

	@Override
	public void translateFile (File inFile,
		File outFile,
		Map<String, String> config,
		FilterContext context,
		ITranslateCallback callback)
		throws Exception
	{
		parseCallback = null;
		translateCallback = callback;
		alignCallback = null;
        try {
        	// For 2.5
        	if ( checkOmegaTVersion() ) {
        		translateCallback.setPass(1);
        	}
            processFile(inFile, outFile, config, context);

        	// For 2.5 only too
            if ( requirePrevNextFields() ) {
            	translateCallback.linkPrevNextSegments();
            	translateCallback.setPass(2);
                processFile(inFile, outFile, config, context);
            }
        }
        finally {
        	translateCallback = null;
        }
	}

    @Override
    public boolean isBilingual() {
        return false;
    }

    /**
     * Method can be overridden for return true. It means what two-pass parsing and translating will be
     * processed and previous/next segments will be linked.
     */
    protected boolean requirePrevNextFields () {
        return false; // Default: all Okapi filters have IDs
        // Take doProcessFor2_5() into account if this changes
    }
	
    protected void processFile (File inFile,
       	File outFile,
       	Map<String, String> config,
		FilterContext context)
    	throws IOException, TranslationException
    {
    	inputFile = inFile;
		//LOGGER.info("Original file to process: "+inputFile.getPath());
		//LOGGER.info("Source root: "+context.getProjectProperties().getSourceRoot());
    	//filterContext = context;
    	// Get the source and target locales
    	LocaleId srcLoc = LocaleId.ENGLISH;
    	if ( context.getSourceLang() != null ) {
    		srcLoc = LocaleId.fromString(context.getSourceLang().getLanguage());
    	}
    	LocaleId trgLoc = LocaleId.FRENCH;
    	if ( context.getTargetLang() != null ) {
    		trgLoc = LocaleId.fromString(context.getTargetLang().getLanguage());
    	}

    	// Get the source and target encoding
    	String inEncoding = context.getInEncoding();
    	if ( inEncoding == null ) { // auto
    		inEncoding = defaultInEncoding;
    	}
		lastInEncoding = inEncoding;
    	String outEncoding = context.getOutEncoding();
    	if ( outEncoding == null ) { // auto
    		if ( filterConfigId.startsWith("okf_ttx") ) outEncoding = "UTF-16";
    		else outEncoding = defaultOutEncoding;
    	}
    	
    	String configIdToUse = filterConfigId;
    	boolean includeTUName = true; // By default we include the TU name in the comments
    	boolean protectFinal = true; // By default we protect state=final entries
    	boolean protectNoTrans = false; // By default we just do not show non-translatable
    	boolean isMultilingual = false;
    	boolean useNameAsId = false;
    	
    	// Check custom filter settings
    	// Important: All custom filter settings files must be in the same folder
    	if ( config != null ) {
    		String useDef = config.get(USE_DEFAULT);
    		if ( VALUE_NO.equals(useDef) ) { // Will be false for both "yes" and null
    			String path = config.get(USE_CUSTOM);
    			if ( Util.isEmpty(path) ) {
    				throw new RuntimeException("Filter configuration requested, but path is empty or null.");
    			}

    			// Check if path is "old" style or "new" style (just name)
    			if ( !path.contains("/") && !path.contains("\\") ) {
    				// It's a "new" style path with just filename so we adjust path to point at project folder
    				String projPath = getProjectPath();
    				if ( projPath != null ){
    					path = projPath + OMEGAT_CONFIG_FOLDER + "/" + path;
    				}
    			}

    			// We have a path to a custom filter settings file
   				// Get the configuration id
   				configIdToUse = Util.getFilename(path, false);
   				if ( fcMapper.getConfiguration(configIdToUse) == null ) {
					// Pre-load all custom-filters in the filters directory
					// in case it is needed for sub-filtering
   					fcMapper.setCustomConfigurationsDirectory(Util.getDirectoryName(path));
   					fcMapper.addCustomConfiguration(configIdToUse);
					fcMapper.updateCustomConfigurations();
    			}
    		}

    		// Yes or null -> true
    		String val = config.get(PROTECT_FINAL);
    		protectFinal = ((val==null) || VALUE_YES.equals(val));

    		val = config.get(INCLUDE_TU_NAME);
    		includeTUName = ((val==null) || VALUE_YES.equals(val));
    		
    		// Yes only -> true; no or null -> false
    		protectNoTrans = VALUE_YES.equals(config.get(PROTECT_NOTRANS));

    		val = config.get(USE_NAME_AS_ID);
    		useNameAsId = ((val==null) || VALUE_YES.equals(val));
    	}

		Segment trgSeg;
		IFilter filter = null;
		IFilterWriter writer = null;
		try {
			filter  = fcMapper.createFilter(configIdToUse);
			if ( filter == null ){
				throw new RuntimeException(String.format("Could not create a filter for the configuration '%s'.", configIdToUse));
			}
			// Make sure sub-filter have access to the same mapper
			filter.setFilterConfigurationMapper(fcMapper);
		
			RawDocument rd = new RawDocument(inFile.toURI(), inEncoding, srcLoc, trgLoc);
			filter.open(rd);
			LOGGER.info("Loading: '"+inFile.toPath()+ "' with "+configIdToUse);
			if ( outFile != null ) {
				writer = filter.createFilterWriter();
				writer.setOptions(trgLoc, outEncoding);
				writer.setOutput(outFile.getAbsolutePath());
			}
			
			while ( filter.hasNext() ) {
				Event event = filter.next();
				if ( event.isTextUnit() ) {
					ITextUnit tu = event.getTextUnit();
					// Skip non-translatable entries
					// (only if the option to just protect them is not set)
					if ( !protectNoTrans && !tu.isTranslatable() ) {
						if ( writer != null ) {
							writer.handleEvent(event);
						}
						continue;
					}

					if (useNameAsId && tu.getName() != null) {
						tu.setId(tu.getName());
					}

					// Get target and properties
					boolean isFuzzy = false;
					boolean approved = false;
					TextContainer tc = null;
					String state = null;
					if ( tu.hasTarget(trgLoc) ) {
						
						tc = tu.getTarget(trgLoc);
						// Check the approved property for fuzzy flag
						if ( tc.hasProperty(Property.APPROVED) ) {
							approved = tc.getProperty(Property.APPROVED).getValue().equals("yes");
							isFuzzy = !approved;
						}

						if ( tc.hasProperty(Property.STATE) ) {
							state = tc.getProperty(Property.STATE).getValue();
						}
						if ( tc.isEmpty() ) tc = null;
						
						// Process alt-trans only when parsing not merging
						if ( writer == null ) {
							processAltTrans(tu, srcLoc, trgLoc);
						}
					}

					trgSeg = null;
					ISegments trgSegs = null;
					if ( tc == null ) {
						// If there is no target we create one empty but segmented
						// It'll be used for merging back
						tc = tu.createTarget(trgLoc, false, IResource.COPY_SEGMENTATION);
					}
					trgSegs = tc.getSegments();

					// Do we need to protect this TU?
					// Yes if the TU is non-translatable
					// or is set to protect state='final' and this is state='final'
					boolean protect = !tu.isTranslatable()
						|| ( protectFinal && net.sf.okapi.filters.xliff.XLIFFFilter.State.FINAL.toString().equals(state) );
					
					// Go through the segments
					for ( Segment srcSeg : tu.getSource().getSegments() ) {
						
						// Get the corresponding target (could be null)
						trgSeg = trgSegs.get(srcSeg.id);

						// Exchange the data with OmegaT
						if ( writer == null ) {
							// Determine the target
							boolean hasTarget = (( trgSeg != null )
								&& !trgSeg.text.isEmpty()
								&& (( trgSeg.getContent().compareTo(srcSeg.getContent(), CompareMode.CODE_DATA_ONLY)!=0 )
									|| approved || impliesXlation(state) ));
							
							// Set the comment
							String comments = processComments(tu, hasTarget, includeTUName, srcSeg, trgSeg, state);
							
							//--- Populate OmegaT

							// Default
							String srcOTCont = toOmegat(srcSeg.text);
							String trgOTCont = (hasTarget ? toOmegat(trgSeg.text) : null);
							final List<ProtectedPart> parts;
							
							if ( protect ) {
								ProtectedPart part = new ProtectedPart();
								part.setReplacementMatchCalculation("");
								part.setReplacementUniquenessCalculation("");
								part.setReplacementWordsCountCalculation("");
								parts = Collections.singletonList(part);
								if ( hasTarget ) {
									// Put the source in the tool-tip
									part.setDetailsFromSourceFile(srcOTCont);
									// Use the target for the content
									srcOTCont = toOmegat(trgSeg.text);
									part.setTextInSourceSegment(srcOTCont);
								}
								else {
									// Use the source for the content, no need for tool-tip
									part.setTextInSourceSegment(srcOTCont);
								}
								
							} else {
								parts = protectedPartsFor(srcSeg);
							}
							
							// Set the OmegaT entry
							parseCallback.addEntry(
								tu.getId()+"_"+srcSeg.id,
								srcOTCont,
								trgOTCont,
								isFuzzy, comments, null,
								this, parts);
						}
						else { // Translation coming back from OmegaT
							// For protected TU: do not use the result from OmegaT
							if ( protect ) {
								// Just move to the next segment
								continue;
							}
							// Else: get the OmegaT translation
							String trans = translateCallback.getTranslation(
								tu.getId()+"_"+srcSeg.id, toOmegat(srcSeg.text));
							// Put it back in the Okapi resource
							if ( !Util.isEmpty(trans) ) {
								if ( trgSeg == null ) {
									// No corresponding target: That should not really occur
								}
								else {
									// Else: make sure we have the inline codes in the target fragment
									trgSeg.text = srcSeg.text.clone();
									// And create the new target content
									fromOmegat(trans, trgSeg.text);
								}
							}
							else { // Empty translation from OmegaT
								if ( isMultilingual ) {
									// Allow empty target for multilingual document types
								}
								else {
									// We use the source, otherwise we may lose data
									trgSeg.text = srcSeg.text;
								}
							}
						}
					}
					if (!tu.getSource().contentIsOneSegment()) {
						tu.getSource().getSegments().joinAll(true);
					}
					if (!tu.getTarget(trgLoc).contentIsOneSegment()) {
						tu.getTarget(trgLoc).getSegments().joinAll(true);
					}
					final TextFragment source = tu.getSource().getFirstContent();
					final TextFragment target = tu.getTarget(trgLoc).getFirstContent();
					final CodeMatches cm = TextFragmentUtil.alignAndCopyCodeMetadata(source, target, true, false);
					if (cm.hasFromMismatch(false) || cm.hasToMismatch(false)) {
						TextFragmentUtil.logCodeMismatchErrors(cm, source, target, tu.getId());
						if (cm.hasToMismatch(false)) {
							for (final int index : cm.getToMismatchIterator()) {
								final Code c = target.getCode(index);
								c.setData(filter.getEncoderManager().encode(c.getData(), EncoderContext.TEXT));
							}
						}
					}
				}
				else if ( event.isStartDocument() ) {
					StartDocument sd = event.getStartDocument();
					isMultilingual = sd.isMultilingual();
				}
				// Write out the translation if needed
				if ( writer != null ) {
					writer.handleEvent(event);
				}
			}
		}
		finally {
			if ( filter != null ) filter.close();
			if ( writer != null ) writer.close();
        	// Close and reset the glossary if needed
        	if ( glosWriter != null ) {
        		glosWriter.close();
        		glosWriter = null;
        	}
        	triedGlossaryCreation = false;
        	// Close and reset the TMX if needed
        	closeTMX();
		}
    	
    }

    /**
     * Indicates if a state implies the target content is a translation (vs a copy of the source).
     * @param state the state value (can be null)
     * @return true if the state implies a translation not a copy.
     */
    private boolean impliesXlation (String state) {
    	if ( state == null ) return false;
    	switch ( state ) {
    	case "final":
    	case "needs-adaptation":
    	case "needs-l10n":
    	case "needs-review-adaptation":
    	case "needs-review-l10n":
    	case "signed-off":
    	case "translated":
    		return true;
    	default:
    		return false;
    	}
    }

    /**
     * Gather annotations to display them as comments
     * @param tu the text unit to process.
     * @param hasTarget true if the target is usable, false if not
     * @param srcSeg the source segment to process.
     * @param trgSeg the corresponding target segment (can be null).
     * @param state the state attribute (can be null).
     * @return The comments or null if there are none.
     */
    protected String processComments (ITextUnit tu,
    	boolean hasTarget,
    	boolean includeTUName,
    	Segment srcSeg,
    	Segment trgSeg,
    	String state)
    {
    	TextFragment tf = srcSeg.getContent();

		StringBuilder tmp = new StringBuilder();
		
		if ( !tu.isTranslatable() ) {
			tmp.append("translate = NO");
		}
		if ( net.sf.okapi.filters.xliff.XLIFFFilter.State.FINAL.toString().equals(state) ) {
			if ( tmp.length() > 0 ) tmp.append("\n");
			tmp.append("state = FINAL");
		}
		else if ( net.sf.okapi.filters.xliff.XLIFFFilter.State.SIGNED_OFF.toString().equals(state) ) {
			if ( tmp.length() > 0 ) tmp.append("\n");
			tmp.append("state = SIGNED-OFF");
		}
		
		// New support for notes after M41
		NoteAnnotation xna = tu.getAnnotation(NoteAnnotation.class);
		if ( xna != null ) {
			for ( Note xn : xna ) {
				if ( Util.isEmpty(xn.getNoteText()) ) continue;
				if ( tmp.length() > 0 ) tmp.append("\n");
				tmp.append(xn.getNoteText());
			}
		}
		
		if ( !Util.isEmpty(tu.getName()) && includeTUName )  {
			if ( tmp.length() > 0 ) tmp.append("\n");
			tmp.append("name = "+tu.getName());
		}
		
		if ( hasTarget ) { // There is a target and we will use it
    		GenericAnnotations anns = trgSeg.getAnnotation(GenericAnnotations.class);
    		if ( anns != null ) {
    			GenericAnnotation ga = anns.getFirstAnnotation(GenericAnnotationType.MTCONFIDENCE);
    			if ( ga != null ) {
    				Double conf = ga.getDouble(GenericAnnotationType.MTCONFIDENCE_VALUE);
    				String ar = ga.getString(GenericAnnotationType.ANNOTATORREF);
    				if ( tmp.length() > 0 ) tmp.append("\n");
    				tmp.append(String.format("MT-Confidence=%s (%s)", Util.formatDouble(conf), ar));
					LOGGER.info("Added MT confidence: " + conf);
    			}
    		}
    	}

    	if ( !tf.hasCode() ) {
    		return tmp.length()==0 ? null : tmp.toString();
    	}
    	
    	String ct = tf.getCodedText();
    	for ( int i=0; i<ct.length(); i++ ) {
    		if ( TextFragment.isMarker(ct.charAt(i)) ) {
    			Code c = tf.getCode(ct.charAt(++i));
    			if ( c.getTagType() == TagType.CLOSING ) continue;
    			GenericAnnotations anns = c.getGenericAnnotations();
    			if ( anns == null ) continue;
    			
    			// Terminology
    			GenericAnnotation ann = anns.getFirstAnnotation(GenericAnnotationType.TERM);
    			if ( ann != null ) {
    				if ( tmp.length() > 0 ) tmp.append("\n");
    				String term = getSpan(ct, i+1, c, tf);
    				tmp.append("Term: \'"+term+"'");
    				String info = ann.getString(GenericAnnotationType.TERM_INFO);
    				if ( info != null ) tmp.append(" "+info);
    				Double conf = ann.getDouble(GenericAnnotationType.TERM_CONFIDENCE);
    				if ( conf != null ) tmp.append(" Confidence="+Util.formatDouble(conf));
    				writeToGlossary(term, info);
    			}

    			// Text Analysis
    			ann = anns.getFirstAnnotation(GenericAnnotationType.TA);
    			if ( ann != null ) {
    				if ( tmp.length() > 0 ) tmp.append("\n");
    				String term = getSpan(ct, i+1, c, tf);
    				tmp.append("TA: \'"+term+"'");
    				StringBuilder info = new StringBuilder();
    				String str = ann.getString(GenericAnnotationType.TA_CLASS);
    				if ( str != null ) info.append(" Class:"+str);
    				str = ann.getString(GenericAnnotationType.TA_IDENT);
    				if ( str != null ) info.append(" Ident:"+str);
    				str = ann.getString(GenericAnnotationType.TA_SOURCE);
    				if ( str != null ) info.append(" Src:"+str);
    				Double conf = ann.getDouble(GenericAnnotationType.TERM_CONFIDENCE);
    				if ( conf != null ) info.append(" Confidence="+Util.formatDouble(conf));
    				tmp.append(info);
    				writeToGlossary(term, info.toString());
    			}
    		}
    	}
    	
    	return ( Util.isEmpty(tmp) ? null : tmp.toString());
    }
    
    private void processAltTrans (ITextUnit tu,
    	LocaleId srcLoc,
    	LocaleId trgLoc)
    {
    	if ( !tu.hasTarget(trgLoc) ) return;
    	// Container level translations
    	TextContainer trgCont = tu.getTarget(trgLoc);
    	AltTranslationsAnnotation anns = trgCont.getAnnotation(AltTranslationsAnnotation.class);
    	if ( anns != null ) {
    		if ( tu.getSource().contentIsOneSegment() ) {
    			writeToTMX(anns, tu.getSource().getFirstContent(), srcLoc, trgLoc);
    		}
    		else {
    			TextContainer srcClone = tu.getSource().clone();
    			srcClone.joinAll();
    			writeToTMX(anns, srcClone.getFirstContent(), srcLoc, trgLoc);
    		}
    	}
    	
    	// Segment level translations
    	ISegments srcSegs = tu.getSourceSegments();
    	ISegments trgSegs = trgCont.getSegments();
    	for ( Segment srcSeg : srcSegs ) {
    		Segment trgSeg = trgSegs.get(srcSeg.getId());
    		if ( trgSeg == null ) continue;
    		anns = trgSeg.getAnnotation(AltTranslationsAnnotation.class);
    		if ( anns != null ) {
        		LOGGER.info("Alternate translations segment level: segId="+trgSeg.getId());
    			writeToTMX(anns, srcSeg.getContent(), srcLoc, trgLoc);
    		}
    	}
    }
	
    /**
     * Gets the text-only span of content for a given open/close code.
     * @param ct the coded text to lookup.
     * @param start the start index in the coded text.
     * @param code the start code. 
     * @param tf the text fragment corresponding to the coded text.
     * @return the span of content (stripped of it's inline codes) that is between
     * the given start code and its ending code.
     */
    private String getSpan (String ct,
    	int start,
    	Code code,
    	TextFragment tf)
    {
    	// Get the index of the closing code
    	int index = tf.getIndexForClosing(code.getId());
    	// If none: return empty span
    	if ( index == -1 ) return "";
    	// Else: look for the corresponding code
    	for ( int i=start; i<ct.length(); i++ ) {
    		if ( TextFragment.isMarker(ct.charAt(i)) ) {
    			if ( index == TextFragment.toIndex(ct.charAt(++i)) ) {
    				// Ending found
    				StringBuilder tmp = new StringBuilder(ct.substring(start, i-1));
    				return TextFragment.getText(tmp.toString());
    			}
    		}
    	}
    	return ""; // Just in case
    }
    
	private String toOmegat (TextFragment tf) {
		return GenericContent.fromFragmentToLetterCoded(tf, true);
	}

	private void fromOmegat (String text,
		TextFragment frag)
	{
		GenericContent.fromLetterCodedToFragment(text, frag, true, true);
	}

	private List<ProtectedPart> protectedPartsFor(final Segment segment) {
		final List<ProtectedPart> protectedParts = new LinkedList<>();
		final String codedText = segment.getContent().getCodedText();
		final List<Code> codes = segment.getContent().getCodes();
		for (int i = 0; i < codedText.length(); i++) {
			if (!TextFragment.isMarker(codedText.charAt(i))) {
				continue;
			}
			final Code code = codes.get(TextFragment.toIndex(codedText.charAt(i + 1)));
			final String tagFormat;
			switch (codedText.codePointAt(i)) {
				case TextFragment.MARKER_OPENING:
					tagFormat = "<g%d>";
					break;
				case TextFragment.MARKER_CLOSING:
					tagFormat = "</g%d>";
					break;
				case TextFragment.MARKER_ISOLATED:
					if (code.getTagType() == TagType.OPENING) {
						tagFormat = "<b%d/>";
					} else if (code.getTagType() == TagType.CLOSING) {
						tagFormat = "<e%d/>";
					} else {
						tagFormat = "<x%d/>";
					}
					break;
				default:
					throw new IllegalStateException("Unsupported code: ".concat(code.toString()));
			}
			protectedParts.add(protectedPartFor(tagFormat, code));
		}
		return protectedParts;
	}

	private ProtectedPart protectedPartFor(final String tagFormat, final Code code) {
		final String tag = String.format(tagFormat, code.getId());
		LOGGER.finest("Tag              : " + tag);
		LOGGER.finest("Code outer data  : " + code.getOuterData());
		LOGGER.finest("Code data        : " + code.getData());
		LOGGER.finest("Code display text: " + code.getDisplayText());
		final ProtectedPart pp = new ProtectedPart();
		pp.setTextInSourceSegment(tag);
		pp.setDetailsFromSourceFile(
			null == code.getDisplayText()
				? code.getOuterData()
				: code.getDisplayText()
		);
		pp.setReplacementWordsCountCalculation(tag);
		pp.setReplacementUniquenessCalculation(tag);
		pp.setReplacementMatchCalculation(tag);
		return pp;
	}

	private boolean checkOmegaTVersion () {
		if ( processOmegaT2_5 == null ) {
			try {
				String tmp = ResourceBundle.getBundle("org/omegat/Version").getString("version");
				processOmegaT2_5 = (tmp.compareTo("2.5.0") >= 0);
				version3_plus = (tmp.compareTo("3.0.0") >= 0);
				LOGGER.info("Omtv= "+tmp+" flag2_5="+processOmegaT2_5+" flag3_plus="+version3_plus);
			}
			catch ( Throwable e ) {
				processOmegaT2_5 = false;
				version3_plus = false;
			}
		}
		return processOmegaT2_5;
	}

	private String getRelativeName (File inputFile) {
        String root = Core.getProject().getProjectProperties().getSourceRoot();
		return inputFile.getPath().substring(root.length());
	}
	
	private void writeToGlossary (String term,
		String info)
	{
		if ( !triedGlossaryCreation && version3_plus ) {
			triedGlossaryCreation = true;
			try {
                String glosRoot = Core.getProject().getProjectProperties().getGlossaryRoot();
				File file = new File(glosRoot, getRelativeName(inputFile) + ".utf8");
				file.getParentFile().mkdirs();
				glosWriter = Util.charsetPrintWriter(file.getAbsolutePath(), StandardCharsets.UTF_8);
			}
			catch ( Throwable e) {
				// Cannot create glossary file
			}
		}
		if ( glosWriter != null ) {
			glosWriter.println(term+"\t"+term); //for tests
			if ( !Util.isEmpty(info) ) {
				glosWriter.println("\t"+info);
			}
		}
	}
	
	private void tryTmxCreation (LocaleId srcLoc,
		LocaleId trgLoc)
	{
		triedTmxCreation = true;
		File file = null;
        String tmRoot = Core.getProject().getProjectProperties().getTMRoot();
		try {
			file = new File(tmRoot, getRelativeName(inputFile) + "_alt.tmx");
			file.getParentFile().mkdirs();
			LOGGER.info("TMX output for alt-trans: "+file.getAbsolutePath());
			tmxWriter = new TMXWriter(file.getAbsolutePath());
			tmxWriter.setLetterCodedMode(true, false);
			tmxWriter.writeStartDocument(srcLoc, trgLoc, null, null, "sentence", null, "unknown");
		}
		catch ( Throwable e) {
			LOGGER.severe("Could not create TMX file " + (file == null ? "<null>" : file.getAbsolutePath()));
    	}
	}

	private void tryTmxMtCreation (LocaleId srcLoc,
		LocaleId trgLoc)
	{
		triedTmxMtCreation = true;
		File file = null;
        String tmRoot = Core.getProject().getProjectProperties().getTMRoot()+"/mt";
		try {
			file = new File(tmRoot, getRelativeName(inputFile) + "_mt.tmx");
			file.getParentFile().mkdirs();
			LOGGER.info("TMX output for alt-trans MT: "+file.getAbsolutePath());
			tmxMtWriter = new TMXWriter(file.getAbsolutePath());
			tmxMtWriter.setLetterCodedMode(true, false);
			tmxMtWriter.writeStartDocument(srcLoc, trgLoc, null, null, "sentence", null, "unknown");
		}
		catch ( Throwable e) {
			LOGGER.severe("Could not create TMX file " + (file == null ? "<null>" : file.getAbsolutePath()));
    	}
	}

	private void writeToTMX (AltTranslationsAnnotation anns,
		TextFragment srcOriginal,
		LocaleId srcLoc,
		LocaleId trgLoc)
	{
		if ( anns == null ) return;
		for ( AltTranslation alt : anns ) {
			if ( alt.getTarget().isEmpty() ) continue; // Skip entries with empty targets
			String origin = alt.getOrigin();
			
			if (( origin != null ) && "BING".equals(origin) ) {
				if ( !triedTmxMtCreation && version3_plus ) {
					tryTmxMtCreation(srcLoc, trgLoc);
				}
				if ( tmxMtWriter == null ) continue;
				tmxMtWriter.writeAlternate(alt, srcOriginal);
			}
			else { // Normal TM
				if ( !triedTmxCreation && version3_plus ) {
					tryTmxCreation(srcLoc, trgLoc);
				}
				if ( tmxWriter == null ) continue;
				tmxWriter.writeAlternate(alt, srcOriginal);
			}
		}
	}
	
	private void closeTMX () {
		if ( tmxWriter != null ) {
			tmxWriter.writeEndDocument();
			tmxWriter.close();
			tmxWriter = null;
		}
		triedTmxCreation = false;
		if ( tmxMtWriter != null ) {
			tmxMtWriter.writeEndDocument();
			tmxMtWriter.close();
			tmxMtWriter = null;
		}
		triedTmxMtCreation = false;
	}
	
	/**
	 * Checks for an open project in OmegaT and returns the path to the project folder
	 * @return path to OmegaT project folder, or null if no project data is found
	 */
	public static String getProjectPath () {
		if (Core.getProject() != null
			&& Core.getProject().getProjectProperties() != null
			&& Core.getProject().getProjectProperties().getProjectRoot() != null)
		{
			return Core.getProject().getProjectProperties().getProjectRoot();
		}
		else {
			return null;
		}
	}
	
	/**
	 * Determines if the project is using global or project-specific filter settings
	 * @return true if using project-specific settings, false if not
	 */
	public static boolean getIsProjectSpecific () {
		if (Core.getProject() != null
			&& Core.getProject().getProjectProperties() != null
			&& Core.getProject().getProjectProperties().getProjectFilters() == null)
		{
			return false; // Using global filter settings
		}
		else {
			return true; // Using project-specific filter settings 
		}
	}

	@Override
	public String getInEncodingLastParsedFile() {
		return lastInEncoding;
	}
}