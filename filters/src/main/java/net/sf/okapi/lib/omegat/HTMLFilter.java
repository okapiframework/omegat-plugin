/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.omegat;

import java.util.Locale;
import java.util.ResourceBundle;

import org.omegat.filters2.Instance;

public class HTMLFilter extends AbstractOkapiFilter {

	private static final String EXTENSION = ".html";
	
	protected static final ResourceBundle res = ResourceBundle.getBundle("omegat-okapi-plugin", Locale.getDefault());

	public HTMLFilter () {
		initialize(res.getString("htmlFilter.name"),
			"okf_html",
			EXTENSION
		);
	}

	@Override
	public Instance[] getDefaultInstances () {
        return new Instance[] {
        	new Instance("*"+EXTENSION)
        };
	}

}
