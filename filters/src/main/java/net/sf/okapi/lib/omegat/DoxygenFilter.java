/*===========================================================================
  Copyright (C) 2015 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.omegat;

import java.util.Locale;
import java.util.ResourceBundle;

import org.omegat.filters2.Instance;

public class DoxygenFilter extends AbstractOkapiFilter {

	private static final String EXTENSION = ".h;*.c;*.cpp;*.java;*.py;*.m";
	
	protected static final ResourceBundle res = ResourceBundle.getBundle("omegat-okapi-plugin", Locale.getDefault());
	
	public DoxygenFilter () {
		initialize(res.getString("doxygenFilter.name"),
			"okf_doxygen",
			EXTENSION);
	}

	@Override
	public Instance[] getDefaultInstances () {
        return new Instance[] {
        	new Instance("*.h"),
        	new Instance("*.c"),
        	new Instance("*.cpp"),
        	new Instance("*.java"),
        	new Instance("*.py"),
        	new Instance("*.m")
        };
	}

}
