/*===========================================================================
  Copyright (C) 2010 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.omegat;

import java.util.Locale;
import java.util.ResourceBundle;

import org.omegat.filters2.Instance;

public class OpenXMLFilter extends AbstractOkapiFilter {

	private static final String EXTENSION = ".docx;*.docm;*.dotx;*.dotm;*.pptx;*.pptm;*.ppsx;*.ppsm;*.potx;*.potm;*.xlsx;*.xlsm;*.xltx;*.xltm;*.vsdx;*.vsdm";

	protected static final ResourceBundle res = ResourceBundle.getBundle("omegat-okapi-plugin", Locale.getDefault());

	public OpenXMLFilter() {
		initialize(res.getString("openXMLFilter.name"), "okf_openxml", EXTENSION);
	}

	@Override
	public Instance[] getDefaultInstances() {
		return new Instance[] {
			new Instance("*.docx"),
			new Instance("*.docm"),
			new Instance("*.dotx"),
			new Instance("*.dotm"),
			new Instance("*.pptx"),
			new Instance("*.pptm"),
			new Instance("*.ppsx"),
			new Instance("*.ppsm"),
			new Instance("*.potx"),
			new Instance("*.potm"),
			new Instance("*.xlsx"),
			new Instance("*.xlsm"),
			new Instance("*.xltx"),
			new Instance("*.xltm"),
			new Instance("*.vsdx"),
			new Instance("*.vsdm")
		};
	}
}
